local Engine = require('engine')
local Components = require('components')

local Walls = Engine.System.newType()

function Walls.new(entities)
  local requiredComponentTypes = {
    Engine.Components.Position,
    Engine.Components.Velocity,
    Components.ScreenConstraint,
  }

  return Engine.System.new(Walls, entities, requiredComponentTypes)
end

function Walls:updateEntity(entity, dt)
  local position = entity:get(Engine.Components.Position)
  local velocity = entity:get(Engine.Components.Velocity)

  if position.x < 0 and velocity.x < 0 then
    position.x = - position.x
    velocity.x = - velocity.x
  end

  if position.x > love.window.getWidth() and velocity.x > 0 then
    position.x = 2 * love.window.getWidth() - position.x
    velocity.x = - velocity.x
  end

  if position.y < 0 and velocity.y < 0 then
    position.y = - position.y
    velocity.y = - velocity.y
  end

  if position.y > love.window.getHeight() and velocity.y > 0 then
    position.y = 2 * love.window.getHeight() - position.y
    velocity.y = - velocity.y
  end
end

return Walls
