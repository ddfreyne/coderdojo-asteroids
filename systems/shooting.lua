local Engine     = require('engine')
local Components = require('components')

local Shooting = Engine.System.newType()

function Shooting.new(entities)
  local requiredComponentTypes = {
    Engine.Components.Position,
    Components.Gun,
  }

  return Engine.System.new(Shooting, entities, requiredComponentTypes)
end

function Shooting:updateEntity(entity, dt)
  local position = entity:get(Engine.Components.Position)
  local gun      = entity:get(Components.Gun)
  local rotation = entity:get(Engine.Components.Rotation)

  if gun.isShooting and gun.cooldown <= 0 then
    local bullet = gun.bulletPrefab.new()
    bullet:add(Engine.Components.Position, position.x, position.y)
    bullet:add(Engine.Components.Rotation, rotation.value)
    local vx = math.cos(rotation.value) * 1000
    local vy = math.sin(rotation.value) * 1000
    bullet:add(Engine.Components.Velocity, vx, vy)
    self.entities:add(bullet)

    gun.cooldown = gun.rate
  else
    gun.cooldown = gun.cooldown - dt
  end
end

return Shooting
