local Engine = require('engine')
local Debugger = require('debugger')

local Gamestate = require('engine.vendor.hump.gamestate')

local Components = require('components')

local Input = {}
Input.__index = Input

function Input.new(entities)
  return setmetatable({ entities = entities }, Input)
end

function Input:update(dt)
  local entity = self.entities:firstWithComponent(Engine.Components.Input)
  if not entity then return end

  -- Rotate and accelerate
  local rotationComponent = entity:get(Engine.Components.Rotation)
  local velocityComponent = entity:get(Engine.Components.Velocity)
  if love.keyboard.isDown("left") then
    rotationComponent.value = rotationComponent.value - (3 * dt)
  elseif love.keyboard.isDown("right") then
    rotationComponent.value = rotationComponent.value + (3 * dt)
  elseif love.keyboard.isDown("up") then
    local r = rotationComponent.value
    local s = 10
    velocityComponent.x = velocityComponent.x + math.cos(r) * s
    velocityComponent.y = velocityComponent.y + math.sin(r) * s
  end

  -- Fire bullets
  local gun = entity:get(Components.Gun)
  if gun then
    gun.isShooting = love.keyboard.isDown(" ")
  end
end

function Input:keypressed(key, isrepeat)
  local entity = self.entities:firstWithComponent(Engine.Components.Input)
  if not entity then return end

  -- Open debugger
  if key == "tab" then
    Gamestate.push(Debugger.Gamestate.new(self.entities))
    return
  end
end

return Input
