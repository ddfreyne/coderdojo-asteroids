local Engine     = require('engine')
local Components = require('components')

local Lifetime = Engine.System.newType()

function Lifetime.new(entities)
  local requiredComponentTypes = {
    Engine.Components.Lifetime,
    Components.MaxLifetime,
  }

  return Engine.System.new(Lifetime, entities, requiredComponentTypes)
end

function Lifetime:updateEntity(entity, dt)
  local lifetime    = entity:get(Engine.Components.Lifetime)
  local maxLifetime = entity:get(Components.MaxLifetime)

  if lifetime.value > maxLifetime.value then
    self.entities:remove(entity)
  end
end

return Lifetime
