local Engine = require('engine')
local Signal = require('engine.vendor.hump.signal')
local Components = require('components')
local Asteroid   = require('prefabs.asteroid')

local CollisionHandler = {}
CollisionHandler.__index = CollisionHandler

function CollisionHandler.new(entities)
  local t = {
    entities   = entities,
    collisions = {},
    callbacks  = {},
  }

  local collisionDetected = function(entityA, entityB)
    table.insert(t.collisions, { entityA, entityB })
  end
  t.callbacks.collisionDetected = collisionDetected
  -- TODO: Do not hardcode this constant
  Signal.register('game:systems:collision:detected', collisionDetected)

  return setmetatable(t, CollisionHandler)
end

function CollisionHandler:leave()
  Signal.remove('game:systems:collision:detected', self.callbacks.collisionDetected)
end

function CollisionHandler:update(dt)
  for _, pair in ipairs(self.collisions) do
    self:pairDetected(pair[1], pair[2])
  end
  self.collisions = {}
end

function CollisionHandler:pairDetected(a, b)
  if self:pairInteracts(a, b) then
    self:singleDetected(a, b)
  end
end

function CollisionHandler:pairInteracts(a, b)
  local aCollisionGroup = a:get(Components.CollisionGroup)
  local bCollisionGroup = b:get(Components.CollisionGroup)

  if not aCollisionGroup then return false end
  if not bCollisionGroup then return false end

  return aCollisionGroup.name ~= bCollisionGroup.name
end

function CollisionHandler:_createAsteroid(x, y, size, vel)
  local sizeString = ''
  if size == 3 then sizeString = 'big'   end
  if size == 2 then sizeString = 'med'   end
  if size == 1 then sizeString = 'small' end

  local asteroid = Asteroid.new()
  asteroid:add(Engine.Components.Image,    'assets/Space shooter/Meteors/meteorBrown_' .. sizeString .. '1.png')
  asteroid:add(Engine.Components.Position, x, y)
  asteroid:add(Components.Split,           size)
  asteroid:add(Engine.Components.Velocity, vel.x, vel.y)
  self.entities:add(asteroid)
end

function CollisionHandler:singleDetected(entity, otherEntity)
  local health        = entity:get(Components.Health)
  local position      = entity:get(Engine.Components.Position)
  local split         = entity:get(Components.Split)

  local otherVelocity = otherEntity:get(Engine.Components.Velocity)
  local otherPosition = otherEntity:get(Engine.Components.Position)

  health.cur = health.cur - 1
  if health.cur <= 0 then
    self.entities:remove(entity)

    if split and split.size > 1 then
      -- Find normals to vector from entity to otherEntity
      local vector = position:vectorTo(otherPosition)
      local normalVector1 = vector:normal1()
      local normalVector2 = vector:normal2()

      -- Find new positions
      local newP1 = position:dup()
      newP1:addVector(normalVector1)
      local newP2 = position:dup()
      newP2:addVector(normalVector2)

      -- Find new velocities
      local newV1 = position:vectorTo(newP1)
      newV1:scalarMultiply(0.5 + math.random() * 2)
      local newV2 = position:vectorTo(newP2)
      newV1:scalarMultiply(0.5 + math.random() * 2)

      -- Create asteroid 1
      self:_createAsteroid(newP1.x, newP1.y, split.size - 1, newV1)
      self:_createAsteroid(newP2.x, newP2.y, split.size - 1, newV2)
    end
  end
end

return CollisionHandler
