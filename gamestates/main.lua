local ArenaSpace = require('spaces.arena')
local Ship       = require('prefabs.ship')
local Asteroid   = require('prefabs.asteroid')
local Gamestate  = require('engine.vendor.hump.gamestate')
local Components = require('components')

local Engine   = require('engine')

local lw = love.window

local Main = setmetatable({}, { __index = Engine.Gamestate })

local function createEntities()
  local entities = Engine.Types.EntitiesCollection.new()

  local camera = Engine.Entity.new()
  camera:add(Engine.Components.Camera)
  camera:add(Engine.Components.Position, lw.getWidth()/2, lw.getHeight()/2)
  camera:add(Engine.Components.Z, 100)
  entities:add(camera)

  local ship = Ship.new()
  ship:add(Engine.Components.Position, love.window.getWidth() / 2, love.window.getHeight() / 2)
  entities:add(ship)

  local asteroid = Asteroid.new()
  asteroid:add(Engine.Components.Velocity, -80, 67)
  asteroid:add(Engine.Components.Position, 100, 200)
  entities:add(asteroid)

  local asteroid = Asteroid.new()
  asteroid:add(Engine.Components.Velocity, 93, 55)
  asteroid:add(Engine.Components.Position, 700, 650)
  entities:add(asteroid)

  local asteroid = Asteroid.new()
  asteroid:add(Engine.Components.Velocity, -11, -77)
  asteroid:add(Engine.Components.Position, 367, 531)
  entities:add(asteroid)

  return entities

end

function Main.new()
  local t = {}

  local entities = createEntities()

  t.arenaSpace = ArenaSpace.new(entities)
  t.spaces = { t.arenaSpace }

  return setmetatable(t, { __index = Main })
end

return Main
