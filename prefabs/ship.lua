local Engine     = require('engine')
local Components = require('components')
local Bullet     = require('prefabs.bullet')

local Ship = {}

function Ship.new()
  local self = Engine.Entity.new()

  self:add(Engine.Components.Input        )
  self:add(Engine.Components.Description, 'Ship')
  self:add(Engine.Components.Velocity,    0, 0)
  self:add(Engine.Components.Scale,       1)
  self:add(Engine.Components.Rotation,    0)
  self:add(Engine.Components.Z,           0)
  self:add(Engine.Components.Image,       'assets/Space shooter/playerShip1_blue_FIXED.png')
  self:add(Components.Health,             3)
  self:add(Components.Gun,                1/5, 0, false, Bullet)
  self:add(Components.ScreenConstraint)
  self:add(Components.CollisionGroup,     'player')

  return self
end

return Ship
