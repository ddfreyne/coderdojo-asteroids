local Engine     = require('engine')
local Components = require('components')

local Bullet = {}

function Bullet.new()
  local self = Engine.Entity.new()

  self:add(Engine.Components.Description, 'Bullet')
  self:add(Engine.Components.Velocity,    0, 0)
  self:add(Engine.Components.Scale,       1)
  self:add(Engine.Components.Rotation,    0)
  self:add(Engine.Components.Z,           0)
  self:add(Engine.Components.Lifetime)
  self:add(Engine.Components.Image,       'assets/Space shooter/Lasers/laserGreen11_FIXED.png')
  self:add(Components.Health,             1)
  self:add(Components.MaxLifetime,        1.0)
  self:add(Components.CollisionGroup,     'player')

  return self
end

return Bullet
