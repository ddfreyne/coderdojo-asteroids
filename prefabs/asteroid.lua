local Engine     = require('engine')
local Components = require('components')

local Asteroid = {}

function Asteroid.new()
  local self = Engine.Entity.new()

  -- self:add(Engine.Components.Input        )
  self:add(Engine.Components.Description, 'Asteroid')
  self:add(Engine.Components.Velocity,    0, 0)
  self:add(Engine.Components.Scale,       1)
  self:add(Engine.Components.Rotation,    0)
  self:add(Engine.Components.Z,           0)
  self:add(Engine.Components.Image,       'assets/Space shooter/Meteors/meteorBrown_big1.png')
  self:add(Components.Health,             1)
  self:add(Components.Split,              3)
  self:add(Components.ScreenConstraint)
  self:add(Components.CollisionGroup,     'enemy')

  return self
end

return Asteroid
