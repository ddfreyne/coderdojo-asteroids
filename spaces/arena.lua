local InputSystem    = require('systems.input')
local ShootingSystem = require('systems.shooting')
local WallsSystem    = require('systems.walls')
local CollisionHandlingSystem    = require('systems.collision_handling')
local LifetimeSystem = require('systems.lifetime')

local Engine = require('engine')

local Arena = {}

function Arena.new(entities)
  local systems = {
    InputSystem.new(entities),
    Engine.Systems.CollisionDetection.new(entities),
    CollisionHandlingSystem.new(entities),
    WallsSystem.new(entities),
    Engine.Systems.Physics.new(entities),
    ShootingSystem.new(entities),
    Engine.Systems.Rendering.new(entities),
    Engine.Systems.Lifetime.new(entities),
    LifetimeSystem.new(entities),
  }

  return Engine.Space.new(entities, systems)
end

return Arena
